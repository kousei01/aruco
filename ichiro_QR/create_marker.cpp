#include "opencv2/aruco.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include <opencv2/imgproc.hpp>
#include <bits/stdc++.h>

int main()
{
    int markerAmount;
    printf("enter marker amount : ");
    scanf("%d" , &markerAmount);
    
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::Mat marker[markerAmount + 1];

    for(int i=1; i<=markerAmount; i++)
    {
        std::string iString = std::to_string(i);
        std::string name = "id " + iString + ".jpg";
        cv::aruco::drawMarker(dictionary, i, 200, marker[i]);
        // cv::imshow(name, marker[i]);
        cv::imwrite(name, marker[i]);
    }    

    cv::waitKey(0);
}
#ArUco

dictionary = kumpulan marker
id = matriks biner
tau orientasi awal

##Create Marker
sebelum dideteksi. Marker harus diprint.
Gambar marker dibuat dengan fungsi drawMarker()

Contoh:
```
cv::Mat markerImage;
cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
cv::aruco::drawMarker(dictionary, 23, 200, markerImage, 1);
```
objek bernama dictionary dibuat dengan memilih salah satu predefined dictionaries dalam modul aruco. dictionary ini dibuat dari 250 marker dan ukuran marker 6*6 bit (DICT_6X6_250).

Parameter drawmarker():
1. dictionary object
2. id marker. Pada contoh ini marker 23 dari dictionary DICT_6X6_250. 
note: tiap dictionary dibuat dari berbagai jumlah marker yang berbeda. pada kasus ini, id yang valid adalah dari 0-249. 
3. ukuran output marker image. Ukuran harus lebih gede dari ukuran marker yang pada kasus ini 6*6. sebaiknya ukurannya number of bits + border size
4. output image
5. lebar border. Ukurannya ditentukan sebanding dengan jumlah bit.
note : misal nilainya 2, berarti lebar border sama dengan ukuran 2 internal bit

##Marker Detection
return list of detected markers. Setiap marker terdeteksi termasuk:
1. posisi dari 4 sudut dalam gambar
2. id marker

###Tahap deteksi
1. deteksi kandidat marker. cari bentuk kotak di gambar yang jadi kandidat marker. Dimulai dengan melakukan adaptive tresholding untuk mensegmentasi marker. lalu kontur diekstrak dari gambar dalam treshold dan yang tidak mendekati bentuk persegi dibuang
2. Cek apa dia beneran marker dengan menganalisa kode didalamnya.
2.1. Dimulai dengan mengekstrak bit marker dalam tiap marker.
2.2. Pertama, transformasi perspektif dilakukan untuk mendapatkan marker dalam bentuk kanonisnya.
2.3. Lalu, gambar kanonis ini di-treshold dengan Otsu untuk misahin bit hitam dan putih. 
Gambar dibagi dalam sel yang berbeda berdasarkan ukuran marker dan ukuran border, serta jumlah pixel hitam dan putih pada setiap sel dihitung untuk menentukan apa dia itu bit hitam atau putih 
2.4. Terakhir bit-bit dianalisa untuk menentukan jika marker itu dimiliki oleh dictionary spesifik dan teknik koreksi error digunakan saat dibutuhkan.

Deteksi dilakukan dalam fungsi detectMarkers()
Contoh
```cv::Mat inputImage;
...
std::vector<int> markerIds;
std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
cv::Ptr<cv::aruco::DetectorParameters> parameters;
cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
cv::aruco::detectMarkers(inputImage, dictionary, markerCorners, markerIds, parameters, rejectedCandidates);
```
Parameter detectMarkers():
1. gambar yang mau dideteksi markernya
2. dictionary object
3. marker terdeteksi disimpan dalam struktur markerCorners dan markerIds:
- markerCorners adalah daftar corner dari marker terdeteksi. Lanjoot...
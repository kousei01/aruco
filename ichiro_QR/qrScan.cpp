#include <opencv2/opencv.hpp>
#include <bits/stdc++.h>

void display(cv::Mat &inputImage, cv::Mat &bbox)
{
  for(int i = 0 ; i < bbox.rows ; i++)
  {
    line(inputImage, cv::Point2i(bbox.at<float>(i,0),bbox.at<float>(i,1)), cv::Point2i(bbox.at<float>((i+1) % bbox.rows,0), bbox.at<float>((i+1) % bbox.rows,1)), cv::Scalar(255,0,0), 3);
  }
  // cv::namedWindow("Result", CV_WINDOW_NORMAL );
  imshow("Original", inputImage);
}

int main()
{
  cv::VideoCapture cap(0); //capture the video from web cam
  if (!cap.isOpened())  // if not success, exit program
  {
    puts("Cannot open the camera");
    return -1;
  }

  while(true)
  {
    // Read image
    cv::Mat inputImage;
    bool Success = cap.read(inputImage); // read a new frame from video

    if (!Success) //if not success, break loop
    {
      puts("Cannot read a frame from video stream");
      break;
    }
    cv::imshow("Original", inputImage);

    cv::QRCodeDetector qrDecoder;

    cv::Mat bbox, rectifiedImage;

    std::string data = qrDecoder.detectAndDecode(inputImage, bbox, rectifiedImage);
    if(data.length()>0)
    {
      std::cout << "Decoded Data : " << data << std::endl;

      // display(inputImage, bbox);
      for(int i = 0 ; i < bbox.rows ; i++)
      {
        line(inputImage, cv::Point2i(bbox.at<float>(i,0),bbox.at<float>(i,1)), cv::Point2i(bbox.at<float>((i+1) % bbox.rows,0), bbox.at<float>((i+1) % bbox.rows,1)), cv::Scalar(255,0,0), 3);
      }
      cv::imshow("Original", inputImage);

      rectifiedImage.convertTo(rectifiedImage, CV_8UC3);
      cv::namedWindow("Rectified QRCode", CV_WINDOW_NORMAL );
      cv::imshow("Rectified QRCode", rectifiedImage);
      // break;
    }
    else
      std::cout << "QR Code not detected" << std::endl;
    
    if (cv::waitKey(30) == 27)
    {
      puts("esc key is pressed by user");
      break; 
    }
  }

}
